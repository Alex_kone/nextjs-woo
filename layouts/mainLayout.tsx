import Link from 'next/link';
import Header from '@/components/_partials/header';
import Footer from '@/components/_partials/footer';

export interface Props {
  children?: any;
}

const MainLayout = ({ children }: Props): JSX.Element => {
  return (
    <>
      <Header mainMenu={children?.props?.mainMenu} />
      <main>{children}</main>
      <Footer />
    </>
  );
};

export default MainLayout;
