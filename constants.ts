export const CONSTANTS = {
  BACKEND_URL: process.env.HOSTNAME,
  GRAPHQL_URL: process.env.GRAPHQL_ENDPOINT,
};
