const path = require('path');

/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  swcMinify: false,
  images: {
    domains: ['localhost', '127.0.0.1:8000', 'www.rituals.com'],
    formats: ['image/avif', 'image/webp']
  },
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')]
  },
  async rewrites() {
    return [
      {
        source: '/wp-content/:path*',
        destination: `http://127.0.0.1:8000/wp-content/:path*`
      }
    ]
  }
}
